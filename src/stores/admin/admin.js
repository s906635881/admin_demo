import { reactive } from 'vue'
import { defineStore } from 'pinia'

const useAdminStore = defineStore('admin', () => {
  const data = reactive({
    name:"",
    token:"",
    expireDate:"" // 过期时间
  })

  const save = (name, token, expireDate) => {
    data.name = name,
    data.token = token,
    data.expireDate = expireDate
  }

  return{
    data,
    save
  }
},
{
  persist:true
})

export default useAdminStore