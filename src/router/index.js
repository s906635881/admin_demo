import useAdminStore from "@/stores/admin/admin"
import { createRouter, createWebHistory } from "vue-router"
import { ElMessage } from 'element-plus'
import Time from '@/utils/Time.js'
import Local from '@/utils/Local.js'


const router = createRouter({
  history: createWebHistory(),
  routes:[
    {
      path: "/",
      redirect: "/admin"
    },
    {
      path: "/login",
      component: () => import("@/views/admin/login.vue")
    },
    {
      path: "/admin",
      component: () => import("@/views/admin/home.vue"),
      meta: {requiresAuth: true}, // 身份验证
      children:[
        // 管理员列表
        {
          path: "administrator/add",
          component: () => import("@/views/admin/administrator/add.vue")
        },
        {
          path: "administrator/list",
          component: () => import("@/views/admin/administrator/list.vue")
        },
        {
          path: "administrator/edit",
          component: () => import("@/views/admin/administrator/edit.vue")
        },
        // 类别管理列表
        {
          path: "category/list",
          component: () => import("@/views/admin/category/list.vue")
        },
        //文章管理列表
        {
          path: "article/list",
          component: () => import("@/views/admin/article/list.vue")
        },
        {
          path: "article/add",
          component: () => import("@/views/admin/article/add.vue")
        },
        {
          path: "article/edit",
          component: () => import("@/views/admin/article/edit.vue")
        },
        // 导航管理
        {
          path: "nav/list",
          component: () => import("@/views/admin/nav/list.vue")
        }
      ]
    },
  ]
})

// 前置守卫
router.beforeEach((to, from, next) => {
  // 需要身份验证的
  if(to.meta.requiresAuth){
    const adminStore = useAdminStore()
    if(adminStore.data.token === ''){
      ElMessage.error("需要登录")
      router.push("/login")
    }

  // 校验登录是否过期
  let startTime = Time.now()
  let endTiem = adminStore.data.expireDate
  let timeSubResult = Time.timeSub(startTime, endTiem)
  
  if(timeSubResult.expire){
    ElMessage.error("登录已过期，请重新登陆")
    Local.remove("admin")
    router.push("/login")
  }
        
    next()
  }else{
    next()
  }
})

export default router