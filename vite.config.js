import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve:{
    alias:{
      '@':path.resolve(__dirname, 'src')
    }
  },
  server: {
    proxy: {
      // 将 /upload 相关的请求代理到 http://127.0.0.1:8008
      '/upload': {
        target: 'http://127.0.0.1:8008',  // 目标服务器
        changeOrigin: true,                // 是否更改请求的来源
        rewrite: (path) => path.replace(/^\/upload/, '')  // 可选：重写路径
      }
    }
  }
})
